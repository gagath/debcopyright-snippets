# debcopyright-snippets

![Logo](docs/logo-128.png)

A collection of Vim snippets for `d/copyright` to be used with the
[UltiSnips](https://github.com/SirVer/ultisnips) plugin.

![Video demonstration](docs/demo.webm){width=709px}

## Contributing

You are invited to provide feedback and contribute. Either by merge requests or
issues in the [salsa
repository](https://salsa.debian.org/gagath/debcopyright-snippets).

## License

In order to ease contribution back to upstream, the license of this repository
is the same: MIT.
